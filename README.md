# GPS Quasar (gps-quasar)

A Quasar Project

## Installation overview
```bash
git clone https://gitlab.com/ofp-oc-cda2/adrien/gps_quasar.git
npm install
quasar dev
```

create .env file in root of project, and past:
```
VITE_FETCH_BACK=http://yourBackInstance.com:8080
```
Rename the back instance and its port. For example, a local instance should look like this: http://localhost:8080.

If you want install your instance (locally or online), see the back repository at [https://gitlab.com/ofp-oc-cda2/adrien/gps](https://gitlab.com/ofp-oc-cda2/adrien/gps).

## Install the dependencies
```bash
yarn
# or
npm install
```

## Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


## Lint the files
```bash
yarn lint
# or
npm run lint
```



## Build the app for production
```bash
quasar build
```

## Customize the configuration
See [Configuring quasar.config.js](https://v2.quasar.dev/quasar-cli-vite/quasar-config-js).

## note chartJs in vue3:
 - **Tooltip**: hide, label, color, => see [chartjs.org/docs/latest/configuration/tooltip.html](https://www.chartjs.org/docs/latest/configuration/tooltip.html)

## new URL dev testing
`http://localhost:9000/acs/gps_quasar2/#/`

## Check list of development

### bug du 11 mai 2022 (resolved)
the bug comes from account.vue lines 8,9,10
when log, nothing is displayed
when commenting lines, the display is ok
when uncomment, it's ok, but if change the view and come back, it not work.

### checklist done
 - [x] crud operation on user:
    - [x] create `<form>` for create user
    - [x] connect the form and the script
    - [x] create function for popup to modify profil
        - [x] in popup to modify profil, add delete profile
        - [x] in popup to modify profil, add change pass
    - [x] add window to confirm for modify profil and password BEFORE fetch
    - [x] on login and sing up, add error popup if necessary.

 - [x] in the back, put all settings of fields (need Id!)
 - [x] account.vue => loop for get data of `champs`: request back (api) on /users/_idOfUser_/champs/_IdOfField_
 - [x] account.vue => loop necessary informations.
 - [x] close popup to modify profil when FUNCTION SAVE is OK.

 - [x] champAdd = return only new field and after push the new in `authShore`.
 - [x] userReadOne: without calendars.
 - [x] when open popup calendar, fetch calendarGet.

 - [x] add calendar => see reactivity
 - [x] updateCalendar => see reactivity
 - [x] deleteCalendar => see reactivity
 - [x] verify delete:
    - [x] calendar => calendar
    - [x] field => field + calendar
    - [x] user => user + field + calendar
 - [X] clean for end:
    - [X] prepare scss for switch display div.dev
    - [X] see quasar.notify => change messages for real messages (final).
    - [X] the 2 tests pages must be delete (because causes regularly bugs, per example run without back).
 - [X] graphic: input number of days between 7 and 366, the default last period is now, else in past.
 - [X] patch the bug: a **field(`champ`).name: `name`** is forbidden (sinon il réécrit la date du champ dans l'affichage graphique, impossible de mettre des calendar dans le champs).
 - [X] when you are connected, if you modify the password it delete connected data (you must refresh to display). => reload page
 - [X] width of graphic must be equal of page width
 - [X] create doc in app

### checklist to do:
 - [ ] new account and modify pass: check if maj+min+number and min x characters (conform to REAC)
 - [ ] graphic style (bar/line)
 - [ ] vue for edit calendars entities like spreadsheet.
 - [ ] verify jwt auto refresh system. possible not work with the front (or the back?) is on a computer in standby.
 - [ ] add front-tests with selenium.
 - [ ] enhanced search settings in window for edit field data.