const theTest = console.log('import test coucou!');

// entrer: data
/* sortie: [
    {
        name: periodLabel,
        field1: number,
        field2: number,
        field3: number,
    }, ...
] */

/* ////////////////////////////////
RELATIVE TO ALL
//////////////////////////////// */

function funcFieldsInObj(storeFields, periodCode, constFormDate) {
  // entrer: index (=num sem) && constFormDate , sortie: { field: value, field: value, }
  const toReturn = {};
  for (let index1 = 0; index1 < constFormDate.length; index1 += 1) {
    const nameField = storeFields[index1].name;
    let countField = 0;
    for (let index2 = 0; index2 < constFormDate[index1].length; index2 += 1) {
      const calObj = constFormDate[index1][index2];
      if (calObj.dateFormat === periodCode) {
        countField += calObj.value;
      }
    }
    toReturn[nameField] = countField;
  }
  return toReturn;
}

/* ////////////////////////////////
RELATIVE TO WEEK
//////////////////////////////// */

function dateToNumWeek(dateParse) {
  // entrer: date.parse, sortie: n° sem (sem du 1/01/1970 = 0)
  return Math.trunc(((dateParse - (4 * 24 * 60 * 60 * 1000)) / (7 * 24 * 60 * 60 * 1000)));
}

function dateToString(inDate) {
  // entrer: Date, sortie: '0000-00-00'
  // const inDate2 = new Date(inDate);
  const year = inDate.getFullYear();
  let month = inDate.getMonth() + 1; // rappel: janvier = 0
  if (inDate.getMonth() < 9) {
    month = `0${inDate.getMonth() + 1}`;
  }
  let day = inDate.getDate();
  if (inDate.getDate() < 10) {
    day = `0${inDate.getDate()}`;
  }
  return `${year}-${month}-${day}`;
}

function numWeekToLabel(numweek) {
  // entrer: numweek, sortie: '0000-00-00 - 0000-00-00'
  const numweekInMs = ((numweek + 1) * 7 * 24 * 60 * 60 * 1000) - (3 * 24 * 60 * 60 * 1000);
  // lundi
  const theDate = new Date(numweekInMs);
  // dimanche
  const theDateSunday = new Date(numweekInMs + (6 * 24 * 60 * 60 * 1000));
  // console.log(theDate);
  const theDateStr = `du ${dateToString(theDate)} au ${dateToString(theDateSunday)}`;
  return theDateStr;
}

/* ////////////////////////////////
RELATIVE TO funcFormArray
//////////////////////////////// */

function funcFormArray(
  storeFields,
  constFormDate,
  numberPeriods,
  labelExtractor,
  labelExtractor2,
  indexInit = 0,
  arraySens = (a) => a,
) {
  const arrayFormat = [];
  for (let index = indexInit; index < numberPeriods; index += 1) {
    const lastLabel = labelExtractor(index);
    // search obj in constFormDate with same month code
    // entrer: month code, constFormDate , sortie: { field: value, field: value, }
    const constFieldsInObj = funcFieldsInObj(storeFields, labelExtractor2(index), constFormDate);
    // merge key obj
    constFieldsInObj.name = lastLabel;
    arrayFormat.push(constFieldsInObj);
  }
  const arrayFormat2 = arraySens(arrayFormat);
  const arrayFormat3 = [];
  for (let index = 0; index < arrayFormat2.length; index += 1) {
    const element = arrayFormat2[index];
    arrayFormat3.unshift(element);
  }
  return arrayFormat3;
}

function funcFormArrayYear(constFormDate, lastPeriod, numberPeriods, storeFields) {
  return funcFormArray(
    storeFields,
    constFormDate,
    numberPeriods,
    (theIndex) => {
      let lastPeriodYear = Number(lastPeriod);
      for (let index = 0; index < theIndex; index += 1) {
        lastPeriodYear -= 1;
      }
      return `${lastPeriodYear}`;
    }, // labelExtractor
    (theIndex) => {
      let lastPeriodYear = Number(lastPeriod);
      for (let index = 0; index < theIndex; index += 1) {
        lastPeriodYear -= 1;
      }
      return `${lastPeriodYear}`;
    }, // labelExtractor2
  );
}

function funcFormArrayMonth(constFormDate, lastPeriod, numberPeriods, storeFields) {
  return funcFormArray(
    storeFields,
    constFormDate,
    numberPeriods,
    (theIndex) => {
      let lastPeriodYear = Number(lastPeriod.slice(0, 4));
      let lastPeriodMonth = Number(lastPeriod.slice(5, 7));
      for (let index = 0; index < theIndex; index += 1) {
        if (lastPeriodMonth > 1) {
          lastPeriodMonth -= 1;
        } else {
          lastPeriodMonth = 12;
          lastPeriodYear -= 1;
        }
      }
      let lastLabel = `${lastPeriodYear}-${lastPeriodMonth}`;
      if (lastPeriodMonth < 10) {
        lastLabel = `${lastPeriodYear}-0${lastPeriodMonth}`;
      }
      return lastLabel;
    }, // labelExtractor
    (theIndex) => {
      let lastPeriodYear = Number(lastPeriod.slice(0, 4));
      let lastPeriodMonth = Number(lastPeriod.slice(5, 7));
      for (let index = 0; index < theIndex; index += 1) {
        if (lastPeriodMonth > 1) {
          lastPeriodMonth -= 1;
        } else {
          lastPeriodMonth = 12;
          lastPeriodYear -= 1;
        }
      }
      let lastLabel = `${lastPeriodYear}-${lastPeriodMonth}`;
      if (lastPeriodMonth < 10) {
        lastLabel = `${lastPeriodYear}-0${lastPeriodMonth}`;
      }
      return lastLabel;
    }, // labelExtractor2
  );
}

function funcFormArrayWeek(constFormDate, lastPeriod, numberPeriods, storeFields) {
  return funcFormArray(
    storeFields,
    constFormDate,
    lastPeriod + 1,
    (theIndex) => numWeekToLabel(theIndex), // labelExtractor
    (theIndex) => theIndex, // labelExtractor2
    lastPeriod - numberPeriods, // indexInit
    (a) => {
      const toReturn = [];
      for (let index = 0; index < a.length; index += 1) {
        const element = a[index];
        toReturn.unshift(element);
      }
      return toReturn;
    },
  );
}

/* ////////////////////////////////
RELATIVE TO funcFormDate
//////////////////////////////// */

function funcFormDate(data, dateExtractor) {
  const toReturn = [];
  for (let index = 0; index < data.length; index += 1) {
    const toReturnField = [];
    for (let index2 = 0; index2 < data[index].length; index2 += 1) {
      const element = data[index][index2];
      const TheDate = new Date(element.date);
      element.dateFormat = dateExtractor(TheDate);
      toReturnField.push(element);
    }
    toReturn.push(toReturnField);
  }
  return toReturn;
}

function funcFormDateWeek(data) {
  return funcFormDate(data, (date) => {
    const TheDateMs = Date.parse(date);
    return dateToNumWeek(TheDateMs);
  });
}

function funcFormDateMonth(data) {
  return funcFormDate(data, (date) => {
    if (date.getMonth() + 1 < 10) {
      return `${date.getFullYear()}-0${date.getMonth() + 1}`;
    }
    return `${date.getFullYear()}-${date.getMonth() + 1}`;
  });
}

function funcFormDateYear(data) {
  return funcFormDate(data, (date) => `${date.getFullYear()}`);
}
/* ////////////////////////////////
RELATIVE TO agregate
//////////////////////////////// */

function agregateYear(iterateStep, nowDay, data, storeFields) {
  // ajoute dateFormat
  const constFormDate = funcFormDateYear(data);
  // mise en forme array
  // const lastPeriod = '2022'; // généré dynamiquement via ref input

  const lastPeriod = `${nowDay.getFullYear()}`;
  const numberPeriods = iterateStep; // généré dynamiquement via ref input

  const constFormArray = funcFormArrayYear(constFormDate, lastPeriod, numberPeriods, storeFields);
  return constFormArray;
}

function agregateMonth(iterateStep, nowDay, data, storeFields) {
  // ajoute dateFormat
  const constFormDate = funcFormDateMonth(data);
  // mise en forme array
  // const lastPeriod = '2022-09'; // généré dynamiquement via ref input

  let lastPeriod = `${nowDay.getFullYear()}-${nowDay.getMonth() + 1}`;
  if (nowDay.getMonth() + 1 < 10) {
    lastPeriod = `${nowDay.getFullYear()}-0${nowDay.getMonth() + 1}`;
  }

  const numberPeriods = iterateStep; // généré dynamiquement via ref input

  const constFormArray = funcFormArrayMonth(constFormDate, lastPeriod, numberPeriods, storeFields);
  // result
  console.table(constFormArray);
  return constFormArray;
}

function agregateWeek(iterateStep, nowDay, data, storeFields) {
  // console.log('agregateWeek(iterateStep, nowDay, data)', iterateStep, nowDay, data);
  // ajoute dateFormat
  const constFormDate = funcFormDateWeek(data);
  // mise en forme array
  const nowDate = new Date(nowDay);
  nowDate.setDate(nowDate.getDate() + 1);
  const lastPeriod = dateToNumWeek(Date.parse(nowDate)); // généré dynamiquement via ref input
  const numberPeriods = iterateStep; // généré dynamiquement via ref input

  const constFormArray = funcFormArrayWeek(constFormDate, lastPeriod, numberPeriods, storeFields);
  // result
  // console.table(constFormArray);
  return constFormArray;
}

/* ////////////////////////////////
CALL FUNCTIONS
//////////////////////////////// */

/* agregateWeek(data);

agregateMonth(data);

agregateYear(data); */

export {
  theTest,
  agregateWeek,
  agregateMonth,
  agregateYear,
};
