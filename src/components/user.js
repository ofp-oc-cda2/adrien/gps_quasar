const modifUserPass = () => {
  const bodyRequest = {};
  bodyRequest.passOld = updateAccountPassOld.value;
  bodyRequest.passNew = updateAccountPassNew.value;
  bodyRequest.passNewVerify = updateAccountPassNewVerify.value;
  if (bodyRequest.passNew !== bodyRequest.passNewVerify) {
    quasar.notify({
      message: 'les 2 versions du nouveau password diffèrent',
      color: 'negative',
      badgeTextColor: 'white',
      progress: true,
    });
    return false;
  }
  const requestOptionsConn = {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${authStore.jwt}`,
    },
    body: JSON.stringify({
      oldPass: bodyRequest.passOld,
      newPass: bodyRequest.passNew,
    }),
  };
  quasar.dialog({
    title: 'Confirm',
    message: 'êtes vous sur de vouloir appliquer les modifications?',
    cancel: true,
  }).onOk(() => {
    fetch(
      `${import.meta.env.VITE_FETCH_BACK}/users/${authStore.userId}/pass`,
      requestOptionsConn,
    )
      .then((response) => {
        response.json();
        if (response.status !== 200) {
          quasar.dialog({
            title: 'Alert',
            message: 'un problème est survenu ...',
          });
          throw response.status;
        }
        try {
          console.log('that is try');
          console.log(response);
          authStore.userObject.userName = bodyRequest.userName;
          authStore.userObject.mail = bodyRequest.mail;
          authStore.userObject.comment = bodyRequest.comment;
          updateAccountPassOld.value = '';
          updateAccountPassNew.value = '';
          updateAccountPassNewVerify.value = '';
          // authStore.updateUserObject(authStore.userObject);
          popupFullSize.value = false;
          quasar.notify({
            message: 'Les modifications ont été appliqués',
            color: 'positive',
            badgeTextColor: 'white',
            progress: true,
          });
        } catch (e) {
          console.log('error - no valid response');
          console.log(e);
        }
      });
  });
};
