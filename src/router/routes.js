const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: 'hello', component: () => import('pages/hello.vue') },
      { path: 'apropos', component: () => import('pages/apropos.vue') },
      {
        name: 'account',
        // path: 'account/:id',
        path: 'account',
        component: () => import('pages/account.vue'),
        // props: true,
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
