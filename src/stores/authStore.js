import { defineStore } from 'pinia';
import { LocalStorage } from 'quasar';

export const useAuthStore = defineStore('auth', {
  state: () => ({
    loggedIn: LocalStorage.has('jwt') && LocalStorage.has('userId'),
    jwt: LocalStorage.getItem('jwt'),
    userId: LocalStorage.getItem('userId'),
    userObject: null,
  }),
  getters: {
    // getTocken: (state) => state.jwt,
  },
  actions: {
    fetchProfile() {
      if (!this.jwt || !this.userId) {
        return console.log('vous n\'êtes pas connecté!');
      }
      const requestOptionsObjUser = {
        method: 'GET',
        headers: { Authorization: `Bearer ${this.jwt}` },
      };
      fetch(`${import.meta.env.VITE_FETCH_BACK}/users/${this.userId}`, requestOptionsObjUser)
        .then((response) => response.json())
        .then((data) => this.userObject = data);
      // .then(route.push({ name: 'account' }));
    },

    login(name, pass) {
      const requestOptionsConn = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          username: name,
          password: pass,
        }),
      };
      console.log('login before fetch');
      return fetch(`${import.meta.env.VITE_FETCH_BACK}/login`, requestOptionsConn)
        .then((response) => response.json())
        .then((data) => {
          console.log('login reponse then');
          if (data.access_token) {
            LocalStorage.set('jwt', data.access_token);
            LocalStorage.set('userId', data.user_id);
            this.loggedIn = true;
            this.jwt = data.access_token;
            this.userId = data.user_id;
            this.fetchProfile();
            return true;
          }
          console.log('error - no tocken return');
          console.log(data);
          return false;
        });
    },
    logout() {
      this.loggedIn = false;
      this.jwt = null;
      this.userId = null;
      this.userObject = null;
      LocalStorage.clear();
    },
  },
});
